testFibo();

function getFibo(limit, current, addelems) {

    // parameter checking
    if (limit === undefined) limit = 10;
    else if (typeof limit !== "number" || limit < 0) { console.error("invalid argument: limit:",limit); return; }
    if (current === undefined) current = [];
    else if (typeof current !== "object" || !Array.isArray(current) || !current.every(function(v){return isFinite(v)})) { console.error("invalid argument: limit:",current); return; }
    if (addelems === undefined) addelems = [1,2];
    else if (typeof addelems !== "object" || !Array.isArray(addelems) || !addelems.every(function(v){return isInt(v) && v>=1;})) { console.error("invalid argument: addelems:",addelems); return; }

    var max = arrayMax(addelems); // equivalent to Math.max(...addelems)
    if (current.length < max) {
        while(current.length < max) current.push(1);
    }
    if (current.length < limit) {
        var newElem = addelems.reduce(function(c,idx){
            return c + current[current.length-idx];
        },0);
        current.push(newElem);
        return getFibo(limit,current,addelems);
    } else {
        while (current.length > limit) current.pop();
        return current;
    }
}

function arrayMax(a) {
    return a.reduce(function(c,e){ return e>c?e:c; });
}

function isInt(value) {
    return isFinite(value) && Math.ceil(value) === Math.floor(value);
}

function testFibo() {
    // normal fibo with default 10
    console.log("test1: "+testEquals([1,1,2,3,5,8,13,21,34,55],getFibo()));

    // normal fibo with item count 5
    console.log("test2: "+testEquals([1,1,2,3,5],getFibo(5)));

    // <= 2 elements
    console.log("test3: "+testEquals([1,1],getFibo(2)));
    console.log("test4: "+testEquals([1],getFibo(1)));
    console.log("test5: "+testEquals([],getFibo(0)));

    // fibo with unconventional starting values
    console.log("test6: "+testEquals([4,7,11,18],getFibo(4,[4,7])));
    // fibo with different method: adding n-3 and n-1 elements
    console.log("test7: "+testEquals([1,2,3,4,6,9],getFibo(6,[1,2,3],[1,3])));
    // fibo with different method: adding n-3 and n-2 and n-1 elements
    console.log("test8: "+testEquals([1,2,3,6,11,20],getFibo(6,[1,2,3],[1,2,3])));
    // fibo with different method: adding n-3 and n-2 elements (asymmetric)
    console.log("test9: "+testEquals([2,3,4,5,7,9],getFibo(6,[2,3,4],[2,3])));
    // <= 3 elements with additional parameters
    console.log("test10: "+testEquals([2,3,4],getFibo(3,[2,3,4],[2,3])));
    console.log("test11: "+testEquals([2,3],getFibo(2,[2,3,4],[2,3])));
    console.log("test12: "+testEquals([2],getFibo(1,[2,3,4],[2,3])));
    console.log("test13: "+testEquals([],getFibo(0,[2,3,4],[2,3])));

    // test invalid arguments
    console.log("test14: "+testEquals(undefined,getFibo(-1)));
    console.log("test15: "+testEquals(undefined,getFibo("1")));
    console.log("test16: "+testEquals(undefined,getFibo({"limit":1})));

    console.log("test17: "+testEquals(undefined,getFibo(2,"invalid type")));
    console.log("test18: "+testEquals(undefined,getFibo(2,{"current": [1,1]})));
    console.log("test19: "+testEquals(undefined,getFibo(2,[1,NaN,1])));

    console.log("test20: "+testEquals(undefined,getFibo(2,[1,1],"invalid type")));
    console.log("test21: "+testEquals(undefined,getFibo(2,[1,1],{"addelems":[1,1]})));
    console.log("test22: "+testEquals(undefined,getFibo(2,[1,1],[1,NaN])));
    console.log("test23: "+testEquals(undefined,getFibo(2,[1,1],[0,1])));
    console.log("test24: "+testEquals(undefined,getFibo(2,[1,1],[1,1.1])));

    // silly but valid arguments
    console.log("test25: "+testEquals([-1,-1,-2,-3,-5],getFibo(5,[-1,-1])));
    console.log("test26: "+testEquals([1,1,1,1,1],getFibo(5,[1],[1])));
    console.log("test27: "+testEquals([1,1.5,2.5,4,6.5],getFibo(5,[1,1.5],[1,2])));

}

function testEquals(a, b) {
    var jsona = JSON.stringify(a)
    var jsonb = JSON.stringify(b);
    if (jsona === jsonb) return jsona;
    else return jsona+" != "+jsonb;
}
